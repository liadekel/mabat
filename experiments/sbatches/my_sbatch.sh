#!/bin/bash

#############################
###  argument collecting ####
#############################

POSITIONAL=()
while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    -n|--nproc)
    NPROC="$2"
    shift # past argument
    shift # past value
    ;;
    -c|--config)
    CONFIG_NAME="$2"
    shift # past argument
    shift # past value
    ;;
    -p|--prefix-name)
    PREFIX_NAME="$2"
    shift # past argument
    shift # past value
    ;;
    -t|--test)
    TEST="test"
    shift # past argument
    shift # past value
    ;;
    *)    # unknown option
    POSITIONAL+=("$1") # save it in an array for later
    shift # past argument
    ;;
esac
done

##########################
## updating sbatch file ##
##########################
SBATCH_FILE=$BASE_DIR/experiments/sbatches/sbatch.template

# update nproc
sed -i "s|NPROC=.*|NPROC=${NPROC}|" ${SBATCH_FILE}
sed -i "s|#SBATCH --cpus-per-task.*|#SBATCH --cpus-per-task=${NPROC} ### number of CPU cores|" ${SBATCH_FILE}

# update job name
sed -i "s|#SBATCH --job-name.*|#SBATCH --job-name ${PREFIX_NAME}-${CONFIG_NAME} ### job name|" ${SBATCH_FILE}

# update config name
sed -i "s|CONFIG_NAME=.*|CONFIG_NAME=${CONFIG_NAME}|" ${SBATCH_FILE}

##################################################
## Run the actual stbach if not marked as test ##
##################################################
if [ -z  ${TEST} ]
  then
    sbatch ${SBATCH_FILE}
fi
