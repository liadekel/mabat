import pandas as pd

from math import ceil
from neo4j import GraphDatabase


class NeoDataBuilder:
    def __init__(self, credentials, logger):
        host, port, username, password = credentials.values()
        try:
            self.driver = GraphDatabase.driver(
                f"bolt://{host}:{port}", auth=(username, password))
        except Exception:
            self.driver = None
            logger.warn("Failed connecting to neo4j server")

    def get_generic_query(self, query, query_params, list_label=""):
        """
        Return the records of the given query as a DataFrame of BoltResult
        base on demand.

        Input
        -----
            :param query (string): query to run on remote neo4j server.
            :query params (dict): all the query params and their values.
            :param to_df (boolean): wether to return the result as DataFrame.
        """
        with self.driver.session() as session:
            records = session.run(query, **query_params)
            records = self._records_to_df(records)
            if list_label:
                records = list(records[list_label])

            return records

    @staticmethod
    def _records_to_df(records):
        return pd.DataFrame([r.values() for r in records], columns=records.keys())
