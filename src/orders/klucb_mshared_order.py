import random
import numpy as np

from .abstract_order import Order
from algorithms.thompson_sampling import KLUCB


class KLUCBMostSharedOrder(Order):
    """
    Order based on Thompson Sampling algorithm.
    """

    def __init__(self, host_env, logger, order_params):
        super(KLUCBMostSharedOrder, self).__init__(
            host_env, logger, order_params)

    def order(self):
        """
        Generator to determine the order of iteration over objects.
        We say object because it can be only IOCs objects, or attacks pointing
        to IOCs list.
        """
        # number of bandits is as number of attacks
        self.klucb = KLUCB(
            len(self.host_env.elements), {})
        self.items = list(self.host_env.elements.items())
        # shuffle attacks order inplace
        random.shuffle(self.items)
        # round number
        t = 0

        while True:
            selected_attack_index = self.klucb.get_arm(t)
            selected_attack_id, _ = self.items[selected_attack_index]
            # get all attack iocs that are *not* removed
            attack_iocs = self.host_env.get_iocs_left(selected_attack_id)

            if not attack_iocs:
                # we checked all iocs of attack
                # by putting *np.inf* in number of pulls, the beta
                # distribution of this arm will always return 0
                # thus the arm will not be selected anymore.
                self.klucb.pulls[selected_attack_index] = -np.inf
                if selected_attack_id == self.host_env.occuring_attack_id:
                    if self.run_params['early_occuring_attack_stop']:
                        self.stop_condition = True
                continue

            t = t + 1
            # select machine to pull by the number of sharing attacks.
            ioc = self.host_env.get_random_max_shared_ioc(
                selected_attack_id)
            # remove selected ioc from env
            if not self.host_env.remove_ioc_from_attack(
                    ioc.id, selected_attack_id):
                self.logger.warn(
                    f"Failed removng ioc_id={ioc.id} from attack={selected_attack_id}")
            # compute reward for selected attack
            reward = 1 if ioc.occurs else 0
            self.klucb.update(selected_attack_index, reward)
            yield ioc


def get_attack_index(requested_attack_id, lst):
    for idx, attack in enumerate(lst):
        attack_id, _ = attack
        if attack_id == requested_attack_id:
            return idx
