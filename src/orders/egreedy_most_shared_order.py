import random

from IPython import embed
from algorithms.egreedy import EGreedy
from .abstract_order import Order


class EGreedyMostSharedOrder(Order):
    def __init__(self, host_env, logger, order_params):
        super(EGreedyMostSharedOrder, self).__init__(
            host_env, logger, order_params)

    def order(self):
        """
        Generator to determine the order of iteration over objects.
        We say object because it can be only IOCs objects, or attacks pointing
        to IOCs list.
        """
        # number of bandits is as number of attacks
        e = self.order_params["e"]
        self.egreedy = EGreedy(len(self.host_env.elements), e, self.logger)
        self.items = list(self.host_env.elements.items())
        # shuffle attacks order inplace
        random.shuffle(self.items)

        while True:
            selected_attack_index = self.egreedy.select_arm()
            selected_attack_id, _ = self.items[selected_attack_index]
            # get all attack iocs that are *not* removed
            attack_iocs = self.host_env.get_iocs_left(selected_attack_id)

            if not attack_iocs:
                # we checked all iocs of attack
                # hence we put -inf in so it won't be selected anymore
                self.egreedy.values[selected_attack_index] = -1e400
                if selected_attack_id == self.host_env.occuring_attack_id:
                    if self.run_params['early_occuring_attack_stop']:
                        self.stop_condition = True
                continue

            # select machine to pull by the number of sharing attacks.
            ioc = self.host_env.get_random_max_shared_ioc(
                selected_attack_id)
            # remove selected ioc from env
            if not self.host_env.remove_ioc_from_attack(
                    ioc.id, selected_attack_id):
                self.logger.warn(
                    f"Failed removng ioc_id={ioc.id} from attack={selected_attack_id}")
            # compute reward for selected attack
            reward = 1 if ioc.occurs else 0
            self.egreedy.update(selected_attack_index, reward)
            yield ioc

    # def run(self, attack_id, param, run_params):
    #     norm_cost, _ = super(EGreedyMostSharedOrder, self).run(
    #         attack_id, param, run_params)
    #     # we are adding another check, specific for UCB algorithms.
    #     max_ucb_machine_index = self.egreedy.get_random_max_ranked_machine()
    #     occuring_attack_index = get_attack_index(
    #         self.host_env.occuring_attack_id, self.items)
    #     result = (occuring_attack_index == max_ucb_machine_index)

    #     self.logger.debug("attack success result: {} occuring_attack_index={}, max_ranked_attack={}".format(
    #         int(result), occuring_attack_index, max_ucb_machine_index))
    #     return norm_cost, result


def get_attack_index(requested_attack_id, lst):
    for idx, attack in enumerate(lst):
        attack_id, _ = attack
        if attack_id == requested_attack_id:
            return idx
