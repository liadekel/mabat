#!/usr/bin/env python
# coding: utf-8

figures_dir = "/home/liadd/research/figures"

def save_fig(fig, name, end="jpg"):
    full_path = f"{figures_dir}/{name}.{end}"
    fig.savefig(full_path)


import numpy as np
import pandas as pd

from glob import glob
from enum import Enum
from collections import OrderedDict

FREQUENCY = 1
COLUMNS = [0,0.01,0.02,0.04,0.08,0.16,0.32,0.64]
ROWS = [0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9]

num_iocs_df = pd.read_csv("/home/liadd/research/notebook/data-vt-v22/attack_sharing_precentage_vt_v22.csv")

class ConfusionMatrix(Enum):
    RECALL = 0
    PRECISION = 1
    AVG_REWARD = 2
    AP = 3

def get_attack_num_iocs(attack_id):
    df = num_iocs_df[num_iocs_df["attack"] == int(attack_id)]
    return int(df["num_iocs"])

def get_attack_details(attack_id):
    df = num_iocs_df[num_iocs_df["attack"] == int(attack_id)]
    return int(df["num_iocs"]), float(df["sharing_precentage"])

def get_attacks_with_num_iocs(niocs_lower_bound=0, niocs_upper_bound=50000):
    df = num_iocs_df[num_iocs_df["num_iocs"] >= niocs_lower_bound]
    df = num_iocs_df[num_iocs_df["num_iocs"] <= niocs_upper_bound]
    return list(df['attack'])

def get_attacks_in_dir(results_dir):
    attack_ids = set()
    for attack_file in glob(f"{results_dir}/cm_*_*.csv*"):
        attack_id = attack_file.split("_")[-3]
        attack_ids.add(attack_id)
    return attack_ids
    
def get_attack_over_time(results_dir, attack_id, iterations, measure, rfp=None, rfn=None, avg_it=2):
    i = 0
    dfs = {}
    attack_num_iocs = -1
    
    # create data frame for each iteration
    while (i <= iterations):
        if rfp == None and rfp == None:
            dfs[i] = pd.DataFrame(columns=COLUMNS, index=ROWS)
        i += FREQUENCY
    
    for attack_file in glob(f"{results_dir}/cm_*_{attack_id}_*_*.csv"):
        attack_file_df = pd.read_csv(attack_file)
        # each attack is a single configuration, i.e. single fp and fn values.
        for r in attack_file_df.iterrows():
            it = r[1]['iteration']
            fp = r[1]['ex_fp']
            fn = r[1]['ex_fn']
            cm_tp, cm_tn, cm_fp, cm_fn = r[1]['cm_tp'],r[1]['cm_tn'],r[1]['cm_fp'],r[1]['cm_fn']
            
            if (it > iterations):
                # sometimes we will ask for less iterations then available
                # thus no need to keep going
                break
                
            if measure == ConfusionMatrix.AVG_REWARD:
                mes = (cm_tp + cm_fp) / float(it)
            elif measure == ConfusionMatrix.RECALL:
                if attack_num_iocs < 0:
                    attack_num_iocs = get_attack_num_iocs(attack_id)
                mes = cm_tp / float(attack_num_iocs)
            elif measure == ConfusionMatrix.PRECISION:
                mes = cm_tp / float(cm_tp + cm_fp)
            
            # set measure in corresponding df and corresponding fp and fn
            if rfp == None and rfn == None:
                try:
                    # already there is a value
                    if not pd.isna(dfs[it][fp][fn]):
                        if type(dfs[it][fp][fn]) is list:
                            dfs[it][fp][fn].append(mes)
                            if len(dfs[it][fp][fn]) == avg_it:
                                # put mean instead of accumulated values
                                dfs[it][fp][fn] = np.array(dfs[it][fp][fn]).mean()
                        else:
                            # create a new list of values
                            dfs[it][fp][fn] = [dfs[it][fp][fn], mes]
                            if len(dfs[it][fp][fn]) == avg_it:
                                # put mean instead of accumulated values
                                dfs[it][fp][fn] = np.array(dfs[it][fp][fn]).mean()
                    else:
                        dfs[it][fp][fn] = mes
                except:
                    pass
            elif (rfp and rfn == None):
                if rfp == fp:
                    pass
            elif (rfp == None and rfn):
                if rfn == fn:
                    pass
            # measure requested in specific fn/fp
            elif (fp == rfp and fn == rfn):
                dfs[it] = mes
    return dfs

def get_attacks_over_time(results_dir, iterations, measure, avg=False, rfp=None, rfn=None):
    attacks_in_dir = get_attacks_in_dir(results_dir)
    attack_results = {}
    
    for attack in attacks_in_dir:
        print(".", end=" ")
        attack_results[attack] = get_attack_over_time(results_dir, attack_id=attack, iterations=iterations,
                                                     measure=measure, rfp=rfp, rfn=rfn)
    temp = []
    if avg:
        if rfp == None and rfn == None:
            for k,v in attack_results.items():
                v_array = list(v.values())
                v_array = np.array([d.to_numpy() for d in v_array])
                
                if len(temp) == 0:
                    temp = v_array
                else:
                    temp += v_array
            return dict(zip(v.keys(), temp / len(attacks_in_dir)))
        else:
            for k,v in attack_results.items():
                v_array = np.array(list(v.values()))
                if len(temp) == 0:
                    temp = v_array
                else:
                    temp += v_array
            return dict(zip(v.keys(), temp / len(attacks_in_dir)))
    else:
        return attack_results
        


# # Aggregated Measures Along Sharing Precentage


import math
import pandas as pd
import numpy as np

from collections import OrderedDict
from glob import glob
from pdb import set_trace
from IPython import embed


def is_found_all(attack_id, attack_file, rfp,rfn):
    num_iocs, sharing_precentage = get_attack_details(attack_id)
    sharing_precentage = round(sharing_precentage, 2)
    df = pd.read_csv(attack_file)
    
    for row in df.iterrows():
        it = row[1]['iteration']
        fp = row[1]['ex_fp']
        fn = row[1]['ex_fn']
        cm_tp, cm_tn, cm_fp, cm_fn = row[1]['cm_tp'],row[1]['cm_tn'],row[1]['cm_fp'],row[1]['cm_fn']
        
        if (fp != rfp or fn != rfn):
            return None
        
        if math.floor(sharing_precentage * num_iocs - (sharing_precentage * num_iocs * fn)) <= cm_tp:
#             print(f"YES - num_iocs={num_iocs}, sp={sharing_precentage}")
            return row
    # in case didn't succeed return the last state.
#     print(f"NO - num_iocs={num_iocs}, sp={sharing_precentage}")
    return row

def get_average_precision(attack_file, attack_id, end_row, attack_exists=True, fp=0, fn=0):
    attack_file_df = pd.read_csv(attack_file)
    end_iteration = end_row[1]['iteration']
    attack_num_iocs, sharing_precentage = get_attack_details(attack_id)
    
    mAP = 0
    prev_cm_tp = 0
    for r in attack_file_df.iterrows():
        it = r[1]['iteration']
        fp = r[1]['ex_fp']
        fn = r[1]['ex_fn']
        cm_tp, cm_tn, cm_fp, cm_fn = r[1]['cm_tp'],r[1]['cm_tn'],r[1]['cm_fp'],r[1]['cm_fn']

        if (it > end_iteration):
            # sometimes we will ask for less iterations then available
            # thus no need to keep going
            break

        if (cm_tp > prev_cm_tp):
            cur_mAP = cm_tp / it
        else:
            cur_mAP = 0
        mAP += cur_mAP
        prev_cm_tp = cm_tp
    
    # if number of iterations is less then number of artifacts
    gtp = it if it < attack_num_iocs else attack_num_iocs
    # if attack now in knowledge baae
    if attack_exists:
        gtp = gtp - gtp*fn
    else:
        gtp = gtp * sharing_precentage - gtp * sharing_precentage*fn
    
    mAP = (1/gtp) * mAP
    return mAP


def get_measure_over_numiocs(results_dir, measure, attack_exists=True, fp=0, fn=0):
    attacks_in_dir = get_attacks_in_dir(results_dir)
    temp = {}
    df = pd.DataFrame(columns=["numiocs", "measure"])
    
    # iterate over all attacks
    for attack in attacks_in_dir:
        num_iocs, sharing_precentage = get_attack_details(attack)
        print(".", end=" ")
        
        # iterate of all attack files
        for attack_file in glob(f"{results_dir}/cm_*_{attack}_*_*.csv"):
            row = is_found_all(attack, attack_file, fp,fn)
            # if found
            if (row is not None):
                # get measure value
                it = row[1]['iteration']
                fp = row[1]['ex_fp']
                fn = row[1]['ex_fn']
                cm_tp, cm_tn, cm_fp, cm_fn = row[1]['cm_tp'],row[1]['cm_tn'],row[1]['cm_fp'],row[1]['cm_fn']                
                if measure == ConfusionMatrix.PRECISION:
                    # TODO: do we used here number of possible of total number of iocs
                    if attack_exists :
                        mes = (cm_tp) / float(math.floor(num_iocs - num_iocs * fn))
                    else:
                        mes = (cm_tp) / float(math.floor(num_iocs*sharing_precentage) - math.floor(num_iocs * sharing_precentage * fn))
                elif measure == ConfusionMatrix.RECALL:
                    if (cm_tp + cm_fp) > 0:
                        mes = float(cm_tp) / (cm_tp + cm_fp) 
                    else:
                        mes = 0
                elif measure == ConfusionMatrix.AVG_REWARD:
                    if it > 0:
                        mes = (cm_tp + cm_fp) / float(it)
                    else:
                        mes = 0
                elif measure == ConfusionMatrix.AP:
                    if it > 0:
                        mes = get_average_precision(attack_file, attack, row, attack_exists=attack_exists,fp=fp, fn=fn)
                    else:
                        mes = 0
                
                r = pd.DataFrame.from_dict({"numiocs":[num_iocs],
                                            "measure": mes})
                df = df.append(r, ignore_index=True)
                

    merged_df = df.groupby("numiocs").mean()
    merged_df.reset_index(level=0, inplace=True)
    merged_df = merged_df.sort_values(by=['numiocs'])
    merged_df = merged_df.groupby(np.arange(len(merged_df))//4).mean()
    return (list(merged_df['numiocs']), list(merged_df['measure']))
            
                    
def get_measure_over_numiocs_all(results_dir, measure, results_base_num, fp, fn,attack_exists=True):
    ucb1rnddf = get_measure_over_numiocs(results_dir=f"{results_dir}_{results_base_num + 7}/",
                       measure=measure, attack_exists=attack_exists,fp=fp, fn=fn)
    rnddf = get_measure_over_numiocs(results_dir=f"{results_dir}_{results_base_num + 3}/",
                       measure=measure, attack_exists=attack_exists,fp=fp, fn=fn)
    saucbmsdf = get_measure_over_numiocs(results_dir=f"{results_dir}_{results_base_num + 4}/",
                       measure=measure, attack_exists=attack_exists,fp=fp, fn=fn)
    saeg01msdf = get_measure_over_numiocs(results_dir=f"{results_dir}_{results_base_num}/",
                       measure=measure, attack_exists=attack_exists,fp=fp, fn=fn)
    ucb1msdf = get_measure_over_numiocs(results_dir=f"{results_dir}_{results_base_num + 6}/",
                       measure=measure, attack_exists=attack_exists,fp=fp, fn=fn)
    rndmsdf = get_measure_over_numiocs(results_dir=f"{results_dir}_{results_base_num + 9}/",
                       measure=measure, attack_exists=attack_exists,fp=fp, fn=fn)
    eg01rnddf = get_measure_over_numiocs(results_dir=f"{results_dir}_{results_base_num + 2}/",
                       measure=measure, attack_exists=attack_exists,fp=fp, fn=fn)
    saucbrnddf = get_measure_over_numiocs(results_dir=f"{results_dir}_{results_base_num + 5}/",
                       measure=measure, attack_exists=attack_exists,fp=fp, fn=fn)
    msdf = get_measure_over_numiocs(results_dir=f"{results_dir}_{results_base_num + 8}/",
                       measure=measure, attack_exists=attack_exists,fp=fp, fn=fn)
    eg01msdf = get_measure_over_numiocs(results_dir=f"{results_dir}_{results_base_num + 1}/",
                       measure=measure, attack_exists=attack_exists,fp=fp, fn=fn)
    oracledf = get_measure_over_numiocs(results_dir=f"{results_dir}_{results_base_num + 10}/",
                       measure=measure, attack_exists=attack_exists,fp=fp, fn=fn)
    print("")
    
    ret_tuple = (ucb1rnddf, rnddf, saucbmsdf, saeg01msdf, 
                 ucb1msdf, rndmsdf, eg01rnddf, saucbrnddf, 
                 msdf, eg01msdf, oracledf)
    

    return ret_tuple

import matplotlib.pyplot as plt
import seaborn as sns

plt.style.use('seaborn-whitegrid')

def plot_aggregated_over_numiocs(results_tuple, measure, title="", fp=0, fn=0, fig_path="", legend=True):
    (ucb1rnddf, rnddf, saucbmsdf, saeg01msdf, 
     ucb1msdf, rndmsdf, eg01rnddf, saucbrnddf, 
     msdf, eg01msdf, oracledf) = results_tuple
    
    fig = plt.figure(figsize=(14,9))
    
    # sns.lineplot(oracledf[0],
    #               oracledf[1],
    #              linewidth=3, label="ORACLE", color="darkred")

    sns.lineplot(saeg01msdf[0],
                  saeg01msdf[1],
                 linewidth=3, label="SAEG01-MS", color="tab:blue")

    sns.lineplot(saucbmsdf[0],
              saucbmsdf[1],
             linewidth=3, label="SAUCB-MS", color="tab:green")

    sns.lineplot(eg01msdf[0],
              eg01msdf[1],
             linewidth=3, label="EG01-MS", color="tab:orange")

    sns.lineplot(ucb1msdf[0],
              ucb1msdf[1],
             linewidth=3, label="UCB1-MS", color="tab:red")

    sns.lineplot(saucbrnddf[0],
              saucbrnddf[1],
             linewidth=3, label="SAUCB-Rnd", color="tab:purple")

    sns.lineplot(ucb1rnddf[0],
              ucb1rnddf[1],
             linewidth=3, label="UCB1-Rnd", color="tab:pink")

    sns.lineplot(eg01rnddf[0],
              eg01rnddf[1],
             linewidth=3, label="EG01-Rnd", color="tab:brown")

    sns.lineplot(msdf[0],
              msdf[1],
             linewidth=3, label="MS", color="tab:olive")

    sns.lineplot(rndmsdf[0],
              rndmsdf[1],
             linewidth=3, label="Rnd-MS", color="tab:gray")

    sns.lineplot(rnddf[0],
              rnddf[1],
             linewidth=3, label="Rnd-Rnd", color="tab:cyan")
        
    plt.xlabel('Number of Associated Artifacts', fontsize=42)
    plt.ylabel(measure, fontsize=42)

    if legend:
        plt.legend(loc='upper center', bbox_to_anchor=(0.5, 1.28),
            ncol=4,  prop={'size': 22})
    
    plt.xticks(fontsize=35)
    plt.yticks([0,0.2,0.4,0.6,0.8,1.0], fontsize=35)
    plt.tight_layout()
    save_fig(fig, fig_path)


# # Default noise

# ### With actual attack


# with_base_num = 670845
# with_results_dir="/home/liadd/research/results/2021_6_16/merged"
# with_ap_sharing_results = get_measure_over_numiocs_all(with_results_dir, 
#                                                         ConfusionMatrix.AP, 
#                                                         with_base_num,
#                                                         0.01,
#                                                         0.1,
#                                                         attack_exists=True)
# plot_aggregated_over_numiocs(with_ap_sharing_results, measure="AP",
#                              title="Average Precision along numiocs (medium CTI quality, with attack)",
#                             fig_path="measures-for-numiocs/ap-with-attack-default-noise")

### Without actual attack


without_base_num = 670856
without_results_dir="/home/liadd/research/results/2021_6_16/merged"
without_ap_sharing_results = get_measure_over_numiocs_all(without_results_dir, 
                                                        ConfusionMatrix.AP, 
                                                        without_base_num,
                                                        0.01,
                                                        0.1,
                                                        attack_exists=False)
plot_aggregated_over_numiocs(without_ap_sharing_results, measure="AP",
                             title="Average Precision along numiocs (medium CTI quality, with attack)",
                            fig_path="measures-for-numiocs/ap-without-attack-default-noise")
