import os
import random
import pickle
import numpy as np
import pandas as pd

from glob import glob
from enum import Enum
from collections import OrderedDict

FREQUENCY = 1
COLUMNS = [0,0.01,0.02,0.04,0.08,0.16,0.32,0.64]
ROWS = [0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9]

pickle_dir="/home/liadd/research/notebook/pickles/full/"
num_iocs_df = pd.read_csv("/home/liadd/research/notebook/data-vt-v22/attack_num_iocs_vt_v22.csv")

# Utilities

class ConfusionMatrix(Enum):
    RECALL = 0
    PRECISION = 1
    AVG_REWARD = 2

def pickle_to_file(file, obj, base_dir=pickle_dir, force=False):
    full_path = f"{base_dir}/{file}.pickle"
    if os.path.exists(full_path) and not force:
        print(f"{full_path} already exists. exits.")
        return
    with open(full_path, 'wb') as of:
        pickle.dump(obj, of)

def pickle_from_file(file, base_dir=pickle_dir):
    full_path = f"{base_dir}/{file}.pickle"
    with open(full_path, 'rb') as infile:
        return pickle.load(infile)

def mutiple_pickle(results_base_num, results_dir):
    ucb1rnddf = pickle_from_file(f"merged_{results_base_num + 7}", results_dir)
    rnddf = pickle_from_file(f"merged_{results_base_num + 3}", results_dir)
    saucbmsdf = pickle_from_file(f"merged_{results_base_num + 4}", results_dir)
    saeg01msdf = pickle_from_file(f"merged_{results_base_num + 0}", results_dir)
    ucb1msdf = pickle_from_file(f"merged_{results_base_num + 6}", results_dir)
    rndmsdf = pickle_from_file(f"merged_{results_base_num + 9}", results_dir)
    eg01rnddf = pickle_from_file(f"merged_{results_base_num + 2}", results_dir)
    saucbrnddf = pickle_from_file(f"merged_{results_base_num + 5}", results_dir)
    msdf = pickle_from_file(f"merged_{results_base_num + 8}", results_dir)
    eg01msdf = pickle_from_file(f"merged_{results_base_num + 1}", results_dir)
    oracledf = pickle_from_file(f"merged_{results_base_num + 10}", results_dir)
    
    ret_tuple = (ucb1rnddf, rnddf, saucbmsdf, saeg01msdf, 
                 ucb1msdf, rndmsdf, eg01rnddf, saucbrnddf, 
                 msdf, eg01msdf, oracledf)
    
    return ret_tuple

def get_attack_num_iocs(attack_id):
    df = num_iocs_df[num_iocs_df["attack"] == int(attack_id)]
    return int(df["num_iocs"])

def get_attacks_with_num_iocs(niocs_lower_bound=0, niocs_upper_bound=50000):
    df = num_iocs_df[num_iocs_df["num_iocs"] >= niocs_lower_bound]
    df = num_iocs_df[num_iocs_df["num_iocs"] <= niocs_upper_bound]
    return list(df['attack'])

def get_attacks_in_dir(results_dir):
    attack_ids = set()
    for attack_file in glob(f"{results_dir}/cm_*_*.csv*"):
        attack_id = attack_file.split("_")[-3]
        attack_id_int = int(attack_id)
        if (attack_id_int == 111132 or attack_id_int == 111125):
            continue
        attack_ids.add(attack_id_int)
    return attack_ids

def avg_element(x):
    if type(x) is not list:
        return round(x, 4)
    else:
        return round((sum(x) / len(x)), 4)

# Average precision
def get_mAP_matrix(results_dir, attack_id, iterations):
    i = 0
    df = pd.DataFrame(columns=COLUMNS, index=ROWS)
    attack_num_iocs = get_attack_num_iocs(attack_id)
    
    for attack_file in glob(f"{results_dir}/cm_*_{attack_id}_*_*.csv"):
        attack_file_df = pd.read_csv(attack_file)
        
        mAP = 0
        prev_cm_tp = 0
        for r in attack_file_df.iterrows():
            it = r[1]['iteration']
            fp = r[1]['ex_fp']
            fn = r[1]['ex_fn']
            cm_tp, cm_tn, cm_fp, cm_fn = r[1]['cm_tp'],r[1]['cm_tn'],r[1]['cm_fp'],r[1]['cm_fn']
        
            if (it > iterations):
                # sometimes we will ask for less iterations then available
                # thus no need to keep going
                break
            
            if (cm_tp > prev_cm_tp):
                cur_mAP = cm_tp / it
            else:
                cur_mAP = 0
            mAP += cur_mAP
            prev_cm_tp = cm_tp
        
        # if number of iterations is less then number of artifacts
        gtp = it if it < attack_num_iocs else attack_num_iocs
        mAP = (1/gtp) * mAP
        if df[fp][fn] is np.nan:
            df[fp][fn] = mAP
        elif type(df[fp][fn]) is list:
            df[fp][fn].append(mAP)
        else:
            df[fp][fn] = [df[fp][fn], mAP]
        
    df = df.applymap(avg_element)
    return df
    

def get_mAP_matrix_all(results_dir, iterations, num=251 ,attacks=[], avg=True):
    attacks_in_dir = get_attacks_in_dir(results_dir)
    
    if len(attacks) > 0:
        selected_attacks = attacks
    elif num == 251:
        selected_attacks = list(attacks_in_dir)
    else:
        attacks_with_num_iocs = random.sample(get_attacks_with_num_iocs(3000, 5000), num)
        selected_attacks = list(attacks_in_dir.intersection(set(attacks_with_num_iocs)))
    
    attack_results = []
    job_num = results_dir.split("_")[-1]
    job_dir = f"{pickle_dir}/{job_num}"
    
    if not os.path.exists(job_dir):
        os.makedirs(job_dir)
    
    for attack in selected_attacks:
        if os.path.exists(f"{job_dir}/attack_{attack}.pickle"):
            attack_mAP = pickle_from_file(f"{job_num}/attack_{attack}")
        else:
            attack_mAP = get_mAP_matrix(results_dir, attack, iterations)
            pickle_to_file(f"{job_num}/attack_{attack}", attack_mAP)
            
        attack_results.append(attack_mAP)
        print(".", end=" ")
        
    if avg:
        attack_results = pd.concat(attack_results).groupby(level=0).mean()
    print(f"Finished {results_dir}")
    

    return attack_results


if __name__ == '__main__':

    ########
    # WITH #
    ########

    # merged_194656 = get_mAP_matrix_all("/home/liadd/research/results/2021_5_5/merged_194656", 10000)
    # pickle_to_file("merged_194656", merged_194656)
    # merged_194657 = get_mAP_matrix_all("/home/liadd/research/results/2021_5_5/merged_194657", 10000)
    # pickle_to_file("merged_194657", merged_194657)
    # merged_194658 = get_mAP_matrix_all("/home/liadd/research/results/2021_5_5/merged_194658", 10000)
    # pickle_to_file("merged_194658", merged_194658)
    # merged_194659 = get_mAP_matrix_all("/home/liadd/research/results/2021_5_5/merged_194659", 10000)
    # pickle_to_file("merged_194659", merged_194659)
    # merged_194660 = get_mAP_matrix_all("/home/liadd/research/results/2021_5_5/merged_194660", 10000)
    # pickle_to_file("merged_194660", merged_194660)
    # merged_194661 = get_mAP_matrix_all("/home/liadd/research/results/2021_5_5/merged_194661", 10000)
    # pickle_to_file("merged_194661", merged_194661)
    # merged_194662 = get_mAP_matrix_all("/home/liadd/research/results/2021_5_5/merged_194662", 10000)
    # pickle_to_file("merged_194662", merged_194662)
    # merged_194663 = get_mAP_matrix_all("/home/liadd/research/results/2021_5_5/merged_194663", 10000)
    # pickle_to_file("merged_194663", merged_194663)
    # merged_194664 = get_mAP_matrix_all("/home/liadd/research/results/2021_5_5/merged_194664", 10000)
    # pickle_to_file("merged_194664", merged_194664)
    # merged_194665 = get_mAP_matrix_all("/home/liadd/research/results/2021_5_5/merged_194665", 10000)
    # pickle_to_file("merged_194665", merged_194665)
    # merged_194666 = get_mAP_matrix_all("/home/liadd/research/results/2021_5_5/merged_194666", 10000)
    # pickle_to_file("merged_194666", merged_194666)

    ###########
    # WITHOUT #
    ###########

    # merged_194671 = get_mAP_matrix_all("/home/liadd/research/results/2021_5_5/merged_194671", 10000, attacks=[66], num=1)
    # pickle_to_file("merged_194671", merged_194671)
    # merged_194673 = get_mAP_matrix_all("/home/liadd/research/results/2021_5_5/merged_194673", 10000)
    # pickle_to_file("merged_194673", merged_194673)
    # merged_452361 = get_mAP_matrix_all("/home/liadd/research/results/2021_5_5/merged_452361", 10000)
    # pickle_to_file("merged_194667", merged_194667)
    # merged_194668 = get_mAP_matrix_all("/home/liadd/research/results/2021_5_5/merged_194668", 10000)
    # pickle_to_file("merged_194668", merged_194668)
    # merged_194669 = get_mAP_matrix_all("/home/liadd/research/results/2021_5_5/merged_194669", 10000)
    # pickle_to_file("merged_194669", merged_194669)
    # merged_194670 = get_mAP_matrix_all("/home/liadd/research/results/2021_5_5/merged_194670", 10000)
    # pickle_to_file("merged_194670", merged_194670)
    # merged_194672 = get_mAP_matrix_all("/home/liadd/research/results/2021_5_5/merged_194672", 10000)
    # pickle_to_file("merged_194672", merged_194672)
    # merged_194674 = get_mAP_matrix_all("/home/liadd/research/results/2021_5_5/merged_194674", 10000)
    # pickle_to_file("merged_194674", merged_194674)
    # merged_194675 = get_mAP_matrix_all("/home/liadd/research/results/2021_5_5/merged_194675", 10000)
    # pickle_to_file("merged_194675", merged_194675)
    # merged_194676 = get_mAP_matrix_all("/home/liadd/research/results/2021_5_5/merged_194676", 10000)
    # pickle_to_file("merged_194676", merged_194676)
    # merged_194677 = get_mAP_matrix_all("/home/liadd/research/results/2021_5_5/merged_194677", 10000)
    # pickle_to_file("merged_194677", merged_194677)

    
    merged_452361 = get_mAP_matrix_all("/home/liadd/research/results/2021_6_1/merged_452361", 10000)
    pickle_to_file("merged_452361", merged_452361)
    merged_452362 = get_mAP_matrix_all("/home/liadd/research/results/2021_6_1/merged_452362", 10000)
    pickle_to_file("merged_452362", merged_452362)
    merged_452363 = get_mAP_matrix_all("/home/liadd/research/results/2021_6_1/merged_452363", 10000)
    pickle_to_file("merged_452363", merged_452363)
    merged_452364 = get_mAP_matrix_all("/home/liadd/research/results/2021_6_1/merged_452364", 10000)
    pickle_to_file("merged_452364", merged_452364)
    merged_452365 = get_mAP_matrix_all("/home/liadd/research/results/2021_6_1/merged_452365", 10000)
    pickle_to_file("merged_452365", merged_452365)
    merged_452366 = get_mAP_matrix_all("/home/liadd/research/results/2021_6_1/merged_452366", 10000)
    pickle_to_file("merged_452366", merged_452366)
    merged_452367 = get_mAP_matrix_all("/home/liadd/research/results/2021_6_1/merged_452367", 10000)
    pickle_to_file("merged_452367", merged_452367)
    merged_452368 = get_mAP_matrix_all("/home/liadd/research/results/2021_6_1/merged_452368", 10000)
    pickle_to_file("merged_452368", merged_452368)
    merged_452369 = get_mAP_matrix_all("/home/liadd/research/results/2021_6_1/merged_452369", 10000)
    pickle_to_file("merged_452369", merged_452369)
    merged_452370 = get_mAP_matrix_all("/home/liadd/research/results/2021_6_1/merged_452370", 10000)
    pickle_to_file("merged_452370", merged_452370)
    merged_452371 = get_mAP_matrix_all("/home/liadd/research/results/2021_6_1/merged_452371", 10000)
    pickle_to_file("merged_452371", merged_452371)

    # merged_452372 = get_mAP_matrix_all("/home/liadd/research/results/2021_6_1/merged_452372", 10000)
    # pickle_to_file("merged_452372", merged_452372)
    # merged_452373 = get_mAP_matrix_all("/home/liadd/research/results/2021_6_1/merged_452373", 10000)
    # pickle_to_file("merged_452373", merged_452373)
    # merged_452374 = get_mAP_matrix_all("/home/liadd/research/results/2021_6_1/merged_452374", 10000)
    # pickle_to_file("merged_452374", merged_452374)
    # merged_452375 = get_mAP_matrix_all("/home/liadd/research/results/2021_6_1/merged_452375", 10000)
    # pickle_to_file("merged_452375", merged_452375)
    # merged_452376 = get_mAP_matrix_all("/home/liadd/research/results/2021_6_1/merged_452376", 10000)
    # pickle_to_file("merged_452376", merged_452376)
    # merged_452377 = get_mAP_matrix_all("/home/liadd/research/results/2021_6_1/merged_452377", 10000)
    # pickle_to_file("merged_452377", merged_452377)
    # merged_452378 = get_mAP_matrix_all("/home/liadd/research/results/2021_6_1/merged_452378", 10000)
    # pickle_to_file("merged_452378", merged_452378)
    # merged_452379 = get_mAP_matrix_all("/home/liadd/research/results/2021_6_1/merged_452379", 10000)
    # pickle_to_file("merged_452379", merged_452379)
    # merged_452380 = get_mAP_matrix_all("/home/liadd/research/results/2021_6_1/merged_452380", 10000)
    # pickle_to_file("merged_452380", merged_452380)
    # merged_452381 = get_mAP_matrix_all("/home/liadd/research/results/2021_6_1/merged_452381", 10000)
    # pickle_to_file("merged_452381", merged_452381)
    # merged_452382 = get_mAP_matrix_all("/home/liadd/research/results/2021_6_1/merged_452382", 10000)
    # pickle_to_file("merged_452382", merged_452382)